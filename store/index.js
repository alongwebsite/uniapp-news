import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)

const stroe = new Vuex.Store({
	state: {
		historyLists: uni.getStorageSync("__history") || [],
		userinfo : uni.getStorageSync('USERINFO') || {}
	},
	mutations: {
		SET_USER_INFO(state, userinfo){
			state.userinfo = userinfo;
		},
		SET_HISTORY_LISTS(state, history) {
			state.historyLists = history
		},
		CLEAR_HISTORY(state){
			state.historyLists = [];
		}
	},
	actions: {
		set_userinfo({commit},userinfo){
			uni.setStorageSync('USERINFO',userinfo);
			commit('SET_USER_INFO',userinfo);
		},
		set_history({
			commit,
			state
		}, history) {
			const list = state.historyLists;
			list.unshift(history);
			uni.setStorageSync('__history',list);
			commit('SET_HISTORY_LISTS', list);
		},
		clearHistory({commit,state}){
			uni.removeStorageSync('__history');
			commit('CLEAR_HISTORY');
		}
	}
})

export default stroe
